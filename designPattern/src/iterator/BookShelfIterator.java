package iterator;

public class BookShelfIterator implements Iterator {

	private int index;
	private BookShelf bookShelf;
	
	public BookShelfIterator(BookShelf bookShelf) {
		this.bookShelf = bookShelf;
		this.index = 0;
	}
	
	@Override
	public boolean hasNext() {
		
		return index<bookShelf.getLength()?true:false;
	}

	@Override
	public Object next() {
		
		return bookShelf.getBookAt(index++);
	}

}
