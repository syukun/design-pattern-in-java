package iterator;

public class IteratorMain {
	public static void main(String[] args) {
		BookShelf bookShelf = new BookShelf(4);
		bookShelf.appendBook(new Book("Book_One"));
		bookShelf.appendBook(new Book("Book_Two"));
		bookShelf.appendBook(new Book("Book_Three"));
		
		Iterator it = bookShelf.iterator();
		while(it.hasNext()) {
			System.out.println(((Book) (it.next())).getName());
		}
	}

}
